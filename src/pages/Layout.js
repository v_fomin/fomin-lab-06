import { Outlet } from "react-router-dom";

export default function Layout() {
  return (
    <>
      <header className="App-header">
        <Outlet />
      </header>
    </>
  );
}
