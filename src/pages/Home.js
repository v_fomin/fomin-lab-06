import { Form, Navigate } from "react-router-dom";
import { useState } from "react";

export default function Home() {
  const CREDENTIALS = "Volodymyr4";
  const COOKIE_LOGIN = "wLoggedIn=true";

  const [loggedIn, setLoggedIn] = useState(document.cookie.includes(COOKIE_LOGIN));
  const [isIncorrectInput, setIsIncorrectInput] = useState(false);

  function onLoginSubmit(event) {
    event.preventDefault();
    const uName = event.target[0].value;
    const pass = event.target[1].value;
    if(uName === CREDENTIALS && uName === pass) {
      document.cookie = COOKIE_LOGIN;
      setLoggedIn(true);
    } else {
      setIsIncorrectInput(true);
      setTimeout(() => setIsIncorrectInput(false), 1000);
    }
  }

  return (
    <>
      <h1>Weather forecast</h1>
      {loggedIn && <Navigate to="/dashboard" replace={true} />}
      <Form action="/dashboard" onSubmit={onLoginSubmit}>
        <div>
          <label>Username: <input name="uname" type="text"></input></label>
        </div>
        <div>
          <label>Password: <input name="pass" type="password"></input></label>
        </div>
        <button className="App-button" type="submit" id="loginButton">Sign in</button>
        {isIncorrectInput && <div className="App-error-popup">Error!<br/>Invalid input</div>}
      </Form>
    </>
  );
}
