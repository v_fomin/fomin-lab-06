import { Navigate } from 'react-router-dom';
import { useState } from 'react';
import logo from '../logo.svg';
import '../Components/WeatherCard';
import WeatherCard from '../Components/WeatherCard';


export default function Dashboard() {
  const COOKIE_LOGIN = "wLoggedIn=true";
  const FORECAST_DAYS = 7;
  const [weather, setWeather] = useState(null);

  function requestWeather({coords: { latitude = '50.4501', longitude = '30.5234' } }) {
    fetch(`https://api.open-meteo.com/v1/forecast?latitude=${latitude}&longitude=${longitude}&daily=weather_code,temperature_2m_max,temperature_2m_min&timezone=auto`)
        .then(res => res.json())
        .then(json => setTimeout(
            () => setWeather(json.daily), 
            500)
        );
  }
  
  if (!document.cookie.includes(COOKIE_LOGIN)) {
    return <Navigate to="/" replace={true} />
  }
  if(!weather) {
    navigator.geolocation?.getCurrentPosition(requestWeather, requestWeather);

    return (
        <>
            <h1>Welcome!</h1>
            <img src={logo} className="App-logo" alt="logo" />
        </>
    );
  } else {
    const weatherBlocks = [];
    for(let i = 0; i < FORECAST_DAYS; i++) {
      weatherBlocks.push(
        <WeatherCard 
          day={weather.time[i]}
          status={weather.weather_code[i]}
          maxTemp={weather.temperature_2m_max[i]}
          minTemp={weather.temperature_2m_min[i]}
        />);
    }
    return (
        <div className='App-dashboard'>
            {weatherBlocks}
        </div>
    );
  }
}
