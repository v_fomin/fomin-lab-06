import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import Layout from './pages/Layout';
import Home from './pages/Home';
import Dashboard from './pages/Dashboard';
import reportWebVitals from './reportWebVitals';
import './App.css';
import { RouterProvider, Route, createBrowserRouter, createRoutesFromElements, Navigate } from "react-router-dom";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<Layout />} action={Layout.action}>
      <Route index element={<Home />} action={Home.action} />
      <Route path="dashboard" element={<Dashboard />} action={Dashboard.action} />
      <Route path="*" element={<Navigate to="/" replace />} />
    </Route>
  )
);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <RouterProvider router={router} className="App"/>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
