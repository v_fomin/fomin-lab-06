
export default function WeatherCard(props) {
    const WEATHER_CODES = {
        "0": <span>Clear sky &#9728;</span>,
        "1": <span>Mainly clear &#127780;</span>, 
        "2": <span>Partly cloudy &#9925;</span>, 
        "3": <span>Overcast &#127781;</span>,
        "45": <span>Fog &#127787;</span>, 
        "48": <span>Depositing rime fog &#127787;</span>,
        "51": <span>Light drizzle &#127782;</span>, 
        "53": <span>Moderate drizzle &#127783;</span>, 
        "55": <span>Dense intensity drizzle &#127783;</span>,
        "56": <span>Light freezing drizzle &#127782;</span>, 
        "57": <span>Dense intensity freezing drizzle &#127783;</span>,
        "61": <span>Slight rain &#127782;</span>, 
        "63": <span>Moderate rain &#127783;</span>, 
        "65": <span>Heavy intensity rain &#127783;</span>,
        "66": <span>Light freezing rain &#127782;</span>, 
        "67": <span>Heavy intensity freezing rain &#127783;</span>,
        "71": <span>Slight snowfall &#127784;</span>, 
        "73": <span>Moderate snowfall &#127784;</span>, 
        "75": <span>Heavy intensity snowfall &#127784;</span>,
        "77": <span>Snow grains &#127784;</span>,
        "80": <span>Slight rain showers &#127782;</span>, 
        "81": <span>Moderate rain showers &#127783;</span>, 
        "82": <span>Violent rain showers &#127783;</span>,
        "85": <span>Slight snow showers &#127784;</span>, 
        "86": <span>Heavy snow showers &#127784;</span>,
        "95": <span>Slight or moderate thunderstorm &#127785;</span>,
        "96": <span>Thunderstorm with slight hail &#127785;</span>,
        "99": <span>Thunderstorm with heavy hail &#127785;</span>
    };
    const MONTHS = [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    const date = new Date(props.day);
  return (
    <div className="App-weather">
        <div className="App-weather-date">{date.getDate()} {MONTHS[date.getMonth()]} {date.getFullYear()}</div>
        <div className="App-weather-status">{WEATHER_CODES[props.status]}</div>
        <div className="App-weather-temp">Temperature:
            <div className="App-weather-max-temp"><i>Max {props.maxTemp} °C</i></div>
            <div className="App-weather-min-temp"><i>Min {props.minTemp} °C</i></div>
        </div>
    </div>
  );
}
